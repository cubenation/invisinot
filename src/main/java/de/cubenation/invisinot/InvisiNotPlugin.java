package de.cubenation.invisinot;

import com.comphenix.protocol.ProtocolLibrary;
import com.comphenix.protocol.events.PacketAdapter;
import de.cubenation.invisinot.adapter.EntityMetadataPacketAdapter;
import org.bukkit.event.HandlerList;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.java.JavaPlugin;

public class InvisiNotPlugin extends JavaPlugin {

    private PacketAdapter packetListener;

    public void onEnable() {
        packetListener = new EntityMetadataPacketAdapter(this);
        ProtocolLibrary.getProtocolManager().addPacketListener(packetListener);
    }

    public void onDisable() {
        ProtocolLibrary.getProtocolManager().removePacketListener(packetListener);
        HandlerList.unregisterAll((Plugin) this);
    }

}
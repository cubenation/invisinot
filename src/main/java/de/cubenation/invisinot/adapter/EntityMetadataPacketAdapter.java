package de.cubenation.invisinot.adapter;

import com.comphenix.protocol.PacketType;
import com.comphenix.protocol.events.PacketAdapter;
import com.comphenix.protocol.events.PacketEvent;
import com.comphenix.protocol.wrappers.WrappedDataWatcher;
import de.cubenation.invisinot.util.WrapperPlayServerEntityMetadata;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;

/**
 * Created by bhruschka on 11.12.16.
 * Project: InvisiNot
 */
public class EntityMetadataPacketAdapter extends PacketAdapter {

    public EntityMetadataPacketAdapter(Plugin plugin) {
        super(plugin, PacketType.Play.Server.ENTITY_METADATA);
    }

    @Override
    public void onPacketSending(PacketEvent event) {
        WrapperPlayServerEntityMetadata packet = new WrapperPlayServerEntityMetadata(event.getPacket().deepClone());
        event.setPacket(packet.getHandle());

        Player player = event.getPlayer();
        if (!player.hasPermission("invisinot.see")) {
            return;
        }

        WrapperPlayServerEntityMetadata metadata = new WrapperPlayServerEntityMetadata(event.getPacket());

        if (metadata.getEntityID() == player.getEntityId()) {
            return;
        }

        WrappedDataWatcher watcher = new WrappedDataWatcher(packet.getMetadata());


        Byte flag = watcher.getByte(0);
//                String oldName = watcher.getString(2);
//                if (oldName != null && !oldName.isEmpty()) {
//                    System.out.println("Name: " + oldName);
//                }

        if (flag != null) {
            // Clone and update it
            watcher.setObject(0, (byte) (flag & 0xdf));

//                            WrappedDataWatcher.Serializer stringSerializer = WrappedDataWatcher.Registry.get(String.class);
//                            WrappedDataWatcher.Serializer byteSerializer = WrappedDataWatcher.Registry.get(Boolean.class);
//                            if ((flag & 0x20) != 0 && oldName != null) {
//                                String newName = "§3[Invis]§r " + oldName;
//
//                                watcher.setObject(new WrappedDataWatcher.WrappedDataWatcherObject(2, stringSerializer), newName);
//                                watcher.setObject(new WrappedDataWatcher.WrappedDataWatcherObject(3, byteSerializer), true);
//                            } else {
//                                watcher.setObject(new WrappedDataWatcher.WrappedDataWatcherObject(2, stringSerializer), oldName != null ? oldName : "");
//                                watcher.setObject(new WrappedDataWatcher.WrappedDataWatcherObject(3, byteSerializer), false);
//                            }

            packet.setMetadata(watcher.getWatchableObjects());
        }

        event.setPacket(packet.getHandle());
    }
}
